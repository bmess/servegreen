IMAGE := registry.gitlab.com/bmess/servegreen
TAG := latest

build:
	docker build . -t ${IMAGE}:${TAG}

push:
	docker push ${IMAGE}:${TAG}
run:
	@echo "Serving on port 8080"
	docker run -p 8080:8080 -it ${IMAGE}:${TAG}