package main

// Serve a static HTML page with a green background.  Usable as a test program

import (
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "<html><body bgcolor=\"B7DF7D\"><center><h1>Green!</h1></center></body></html>")
		log.Printf("Got %s request from : %s", r.Method, r.Host)
	})
	port := "8080"
	if len(os.Args) > 1 {
		port = os.Args[1]
	}
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
