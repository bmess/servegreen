# Using the latest version under go alpine
#FROM arm32v7/golang:alpine
FROM golang:alpine as build

# Enable modules
ENV GO111MODULE=on
WORKDIR /go/src/servegreen
COPY . .
RUN go build .
EXPOSE 8080


FROM alpine:latest
COPY --from=build /go/src/servegreen/servegreen .
ENTRYPOINT [ "./servegreen" ]