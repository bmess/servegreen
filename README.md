# servegreen

* displays a static web page
* has a dumbed down log emitted to standard log
    * shows HTTP method & from where
* Pretty green background courtesy of https://htmlcolorcodes.com/
    * Green # B7DF7D
    * Yellow #FFC300
    * Orange #FF5733
    * Red #C70039


    # Build & Run

    ```bash
    # build
    make

    # run
    make run
    ```

    Unless overwritten it will use the environment variables `IMAGE` and `TAG`
